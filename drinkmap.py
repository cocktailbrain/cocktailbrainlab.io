import csv,json

#### Get answer map
#@todo: Would be much easier to parse with flipped axes

inname="Cocktail_Studio_Questions-Drinks.csv"

infile_drinks=open(inname)

drink_rows=csv.reader(infile_drinks)

drinkMap={}
icol={'Name':-1,
      'Ingredient 1':-1,
      'Ingredient 2':-1,
      'Ingredient 3':-1,
      'Ingredient 4':-1,
      'Ingredient 5':-1,
      'Ingredient 6':-1,
      'Ingredient 7':-1,
      'Ingredient 8':-1,
      'Ingredient 9':-1,
      'NameFormat':-1,
      'Blurb':-1,
      'Description':-1,
      'Serving':-1,
      'Garnish':-1}

drinktype=None
for row in drink_rows:
    row=[x.strip() for x in row]

    # skip empty lines
    if not sum([len(x) for x in row]): continue

    if len(row[0]):

        # Add new drink type
        drinktype=row[0]
        if drinktype not in drinkMap:
            drinkMap[drinktype]={}

        # Get column headers
        for n,col in enumerate(row):
            for colname in icol:
                if col==colname:
                    icol[colname]=n
                    break   

        continue

    # Get drink name
    drinkname=row[icol['Name']]

    # Skip 2nd header row
    if drinkname=='': continue

    # Add new entry to dict
    if drinkname not in drinkMap[drinktype]:
        drinkMap[drinktype][drinkname]={}

    # Add values to dict
    for i in icol:
        if i=='Name': continue
        if icol[i]==-1: print(f'Column {i} not found')
        
        drinkMap[drinktype][drinkname][i]=row[icol[i]]
    
        
print('Drink map:')
print(print(json.dumps(drinkMap, indent=4, sort_keys=True)))

with open(inname.replace('.csv','.json'),'w') as outfile:
    json.dump(drinkMap,outfile)
    
