import csv,json


#inname="Cocktail_Studio_Questions-Descriptions-Drinks.csv"
#inname="Cocktail_Studio_Questions-Descriptions-Pastime.csv"
#inname="Cocktail_Studio_Questions-Flavours.csv"
inname="Cocktail_Studio_Questions-Garnish.csv"

infile=open(inname)
csv_rows=csv.reader(infile)
outdict={}

cols={}
for n,row in enumerate(csv_rows):
    
    if not n:
        # Get headers
        print(row)
        for icol,col in [(nx,x) for nx,x in enumerate(row) if len(x.strip())]:
            cols[col]=icol
            
    else:
        # Get rows
        key=row[0]
        outdict[key]={}

        for col in cols:
            if cols[col]==0: continue #Don't need to re-add first column

            outdict[key][col]={}
            entries=[res.strip() for res in row[cols[col]].split('\n')]
            for entry in entries:
                subkey='all'
                subentry=entry
                if ':' in entry:
                    subkey=entry.split(':')[0].strip()
                    subentry=entry.split(':')[1]
                    
                outdict[key][col][subkey]=subentry.strip()

print('\nOut dict:')
for key in outdict:
    print(key,":",outdict[key])


with open(inname.replace('.csv','.json'),'w') as outfile:
    json.dump(outdict,outfile)
