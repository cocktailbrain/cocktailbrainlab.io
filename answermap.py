import csv,collections

#### Get answer map
#@todo: Would be much easier to parse with flipped axes
infile_answers=open("Cocktail_Studio_Questions-AnswerMap.csv")
answers_rows=csv.reader(infile_answers)
answerMap=collections.OrderedDict()
#answerMap={}
anskeys=[]
for n,row in enumerate(answers_rows):
    # Get headers
    if not n:
        #['GoToDrink'], ['DrinkStyle'], ['FavouriteTasteProfile'], ['FavouriteFruitType'], ['FavouriteCuisine'], ['FavouritePastime']]
        iGT=row.index('GoToDrink') if 'GoToDrink' in row else -1
        iDS=row.index('DrinkStyle') if 'DrinkStyle' in row else -1
        iFT=row.index('FavouriteTasteProfile') if 'FavouriteTasteProfile' in row else -1
        iFF=row.index('FavouriteFruitType') if 'FavouriteFruitType' in row else -1
        iFC=row.index('FavouriteCuisine') if 'FavouriteCuisine' in row else -1
        iFP=row.index('FavouritePastime') if 'FavouritePastime' in row else -1
        iHE=row.index('HowExperimental') if 'HowExperimental' in row else -1


        for n,col in enumerate(row):
            answerMap[col]={"n":n,"ans":[],"multi":False}
            if col == 'GoToDrink':
                answerMap[col]["full"]='Go-to drink'
            if col == 'DrinkStyle':
                answerMap[col]["full"]='Favourite drink style'
            if col == 'FavouriteTasteProfile':
                answerMap[col]["full"]='Favourite taste profile'
                answerMap[col]["multi"]=True
            if col == 'FavouriteFruitType':
                answerMap[col]["full"]='Favourite fruit type'
            if col == 'FavouriteCuisine':
                answerMap[col]["full"]='Favourite cuisine'
            if col == 'FavouritePastime':
                answerMap[col]["full"]='Favourite pastime'
            if col == 'HowExperimental':
                answerMap[col]["full"]='How experimental are you'
            
        anskeys=list(answerMap.keys())
    else:
        for n,col in enumerate(row):
            tmpkey=None
            for key in answerMap:
                if n==anskeys.index(key):
                    tmpkey=key
                    break
                
            if col != '' and col not in  answerMap[tmpkey]["ans"]:
                answerMap[tmpkey]["ans"].append(col)

print('\nAnswer Map:')
for ans in answerMap:
    print(ans,":",answerMap[ans]["ans"])
