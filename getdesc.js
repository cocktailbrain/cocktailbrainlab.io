'use strict';

const fs = require('fs');

let rawdrinks= fs.readFileSync('Cocktail_Studio_Questions-Drinks.json');
let drinks = JSON.parse(rawdrinks);

let rawpastimes= fs.readFileSync('Cocktail_Studio_Questions-Descriptions-Pastime.json');
let pastimes = JSON.parse(rawpastimes);

let rawflavours= fs.readFileSync('Cocktail_Studio_Questions-Flavours.json');
let flavours = JSON.parse(rawflavours);
//console.log(flavours)

let rawgarnishes= fs.readFileSync('Cocktail_Studio_Questions-Garnish.json');
let garnishes = JSON.parse(rawgarnishes);


var drinktype="Spritz"

var spirit="Vodka"
//var fruit="Citrus"
var fruit="Berry"
//var fruit=""
var cuisine="French"
var pastime="Party holiday"
var tastes=[]
tastes.push("Refreshing")
tastes.push("Herb")


function getdesc(drinktype,spirit,fruit,cuisine,pastime,tastes){

    // Print out inputs
    console.log("Drink type: "+drinktype+"\n")
    console.log("Spirit: "+spirit)
    console.log("Fruit: "+fruit)
    console.log("Cuisine: "+cuisine)
    console.log("Pastime: "+pastime)
    console.log("Tastes: "+tastes)
    console.log("\n")
    
    var highlights=[]
    
    // Get refined fruit value from fruit-cuisine combo
    var fruit_refined=''
    if (fruit == '' && cuisine != ''){
	fruit=flavours[cuisine]['FruitType']['all']
	console.log("Found fruit type for "+cuisine+": "+fruit)
	highlights.push(fruit)
    }
    
    if (fruit != '' && cuisine != ''){
	fruit_refined=flavours[cuisine][fruit]['all']!='' ? flavours[cuisine][fruit]['all'] : fruit
	
	if(fruit!=fruit_refined){
	    console.log("Found refined fruit for "+fruit+" from "+cuisine+": "+fruit_refined)
	    if(highlights.length){
		highlights[0]=fruit_refined
	    }else{
		highlights.push(fruit_refined)
	    }
	}else{
	    console.log("Didn't find refined fruit for "+fruit+" from "+cuisine)
	}
    }
    
    // Get refined taste from taset-cuisine combo
    var tastes_refined=[]
    for(var t=0;t<tastes.length;t++){
	var taste=tastes[t]
	if (taste != '' && cuisine != ''){
	    //console.log(flavours[cuisine])
	    var taste_refined = taste in flavours[cuisine] ? flavours[cuisine][taste]['all'] : ''
	    if ( taste_refined != '' ){
		tastes_refined.push(taste_refined)
		console.log("Found refined taste for "+taste+" from "+cuisine+": "+taste_refined)
	    }else{
		tastes_refined.push(taste)
	    }
	}
    }
    //console.log(tastes_refined)
    
    // Build drink subcategorisation
    var drinkname=fruit+" "+spirit
    //console.log(drinks[drinktype][drinkname])
    
    // Build final summary name
    var screenname=drinks[drinktype][drinkname]['NameFormat'].replace(/,/g,'')
    console.log(screenname)
    screenname=screenname.replace(/__DRINKNAME__/g, drinktype).replace(/__SPIRIT__/g, spirit).replace(/__FRUIT__/g, fruit).replace(/__FRUITREFINED__/g, fruit_refined)
    console.log(screenname)
    
    // Get cocktail blurb
    var blurb=drinks[drinktype][drinkname]['Blurb']
    
    
    // Extract ingredient template list
    var rawingredients=drinks[drinktype][drinkname]['Description'].split(",");
    //console.log(rawingredients)
    var normingredients=[]
    var specingredients=[]
    
    let specialtastes=["Umami", "Spice", "Herb"]
    
    // Replace template values with real ingredients
    for(var i=0;i<rawingredients.length;i++){
	var ingredient=rawingredients[i].trim()
	var inumber=ingredient.match(/\d+/);
	
	ingredient=ingredient.replace(ingredient,drinks[drinktype][drinkname]['Ingredient '+inumber].trim())
	//console.log(ingredient)
	
	// Drop anything in brackets
	if(ingredient.split("(")){
	    ingredient=ingredient.split("(")[0].trim()
	}
	
	// Special treatment of special tastes - only include them if selected in tastes
	if(specialtastes.includes(ingredient)){
	    for(var t=0; t<tastes.length; t++){
		if(tastes[t]==ingredient){
		    // Switch for refined taste
		    console.log("Made special replacement for "+ingredient+": "+tastes_refined[t])    
		    ingredient=ingredient.replace(ingredient,tastes_refined[t])
		    specingredients.push(ingredient)
		    highlights.push(ingredient)
		    break
		}
	    }
	}else{
	    normingredients.push(ingredient)
	}
    }
    
    //@TODO: Fix so that special ingredients come last
    
    console.log("Normal Ingredients:")
    console.log(normingredients)

    console.log("Special Ingredients:")
    console.log(specingredients)
    
    
    // Start building final description
    var ingredientdesc=""
    
    // Get the formatting/grammar right
    for(var i=0;i<normingredients.length;i++){
	if(i==0){
	    ingredientdesc=ingredientdesc+normingredients[i]
	}else if(i<normingredients.length-1){
	    ingredientdesc=ingredientdesc+", "+normingredients[i]
	}else{
	    ingredientdesc=ingredientdesc+" and "+normingredients[i]
	}
    }
    
    for(var i=0;i<specingredients.length;i++){
	if(i==0){
	    ingredientdesc=ingredientdesc+' with a hint of '
	    ingredientdesc=ingredientdesc+specingredients[i]
	}else if(i<specingredients.length-1){
	    ingredientdesc=ingredientdesc+", "+specingredients[i]
	}else{
	    ingredientdesc=ingredientdesc+" and "+specingredients[i]
	}
    }
    ingredientdesc=ingredientdesc+'.'
    
    
    // Replace fruit and fruit_refined everywhere
    ingredientdesc=ingredientdesc.replace(/__FRUIT__/g, fruit).replace(/__FRUITREFINED__/g, fruit_refined)
    
    
    // Add pre-blurb
    ingredientdesc='This delicious cocktail is made from '+ingredientdesc
    
    
    // Get serving suggestion
    var serving=drinks[drinktype][drinkname]['Serving']
    
    
    // Get garnish suggestion
    var garnishdesc=drinks[drinktype][drinkname]['Garnish'].replace(/__FRUIT__/g, fruit).replace(/__FRUITREFINED__/g, fruit_refined)
    
    for(var garnish in garnishes[fruit]){
	//console.log("__"+garnish.toUpperCase()+"__"+" --> "+garnishes[fruit][garnish]['all'])
	garnishdesc=garnishdesc.replace("__"+garnish.toUpperCase()+"__",garnishes[fruit][garnish]['all'])
    }
    
    // Say why different ingredients were chosen
    var why='We selected '
    
    var fruitsuff=["Tropical", "Citrus"]
    
    for(var h=0;h<highlights.length;h++){
	// Name adjustment for some fruits
	if(fruitsuff.includes(highlights[h]))highlights[h]=highlights[h]+" fruit"
	
	if(h==0){
	    why=why+highlights[h]
	}else if(h<highlights.length-1){
	    why=why+", "+highlights[h]
	}else{
	    why=why+" and "+highlights[h]
	}
    }
    why=why+' because of your love of '+cuisine+' cuisine.'
    
    
    // Add pastime sentence
    function getRandomPastime(obj) {
	var n=0
	Object.keys(obj).forEach(function(key) {
	    if (obj[key]['all']!=''){
		n=n+1
	    }
	})
	var rand=Math.random()
	var n_rand=Math.floor(rand * Math.floor(n))+1
	//console.log(rand,n,n_rand)
	return obj['Pastime '+n_rand]['all']
    }
    var pastimedesc = getRandomPastime(pastimes[pastime]).replace(/__DRINKNAME__/g, drinktype)
    
    
    // Builf full description
    var desc=screenname+'\n\n'+blurb+'\n\n'+ingredientdesc+"\n\n"+serving+" and add "+garnishdesc+" to garnish."+"\n\n"+why+"\n\n"+pastimedesc
    console.log("\n")
    console.log(desc)
    console.log("\n")
    
    return desc
}

getdesc(drinktype,spirit,fruit,cuisine,pastime,tastes)
